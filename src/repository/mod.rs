use proto::reward::Reward;
use proto::common::{RewardId, UserId};
use anyhow::Result;
use thiserror::Error;



pub mod inmemory;

#[derive(Error, Debug)]
pub enum DataStoreError {
    #[error("could not find {id:?}")]
    NotFound {
        id : i32
    },
   // #[error("unknown data store error")]
   // Unknown,
}

pub trait RewardRepository {
    fn get_reward(&self, reward_id: RewardId) -> Result<proto::reward::Reward,DataStoreError>;
    fn delete_reward(& self, reward_id: RewardId) -> Result<(),DataStoreError>;
    fn update_reward(& self, reward: Reward) -> Result<proto::reward::Reward,DataStoreError>;
    fn add_reward(& self, reward: Reward) -> Result<proto::reward::Reward,DataStoreError>;
    fn get_all_rewards(&self, user_id: UserId) -> Result<proto::reward::RewardList,DataStoreError>;
}