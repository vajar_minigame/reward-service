use std::collections::HashMap;
use std::result::Result;
use std::sync::{atomic, RwLock};
use std::sync::atomic::AtomicI32;

use proto::common::{RewardId, UserId};
use proto::reward::{Reward, RewardList};

use crate::repository::{DataStoreError, RewardRepository};

pub struct Store {
    rewards: RwLock<HashMap<i32, Reward>>,
    max_id: std::sync::atomic::AtomicI32,
}

impl Store {
    // This is a static method
    // Static methods don't need to be called by an instance
    // These methods are generally used as constructors

    pub fn new() -> Store {
        return Store {
            rewards: RwLock::new(HashMap::new()),
            max_id: AtomicI32::new(0),
        };
    }
}

impl RewardRepository for Store {
    fn get_reward(&self, reward_id: RewardId) -> Result<proto::reward::Reward, DataStoreError> {
        let guard = self.rewards.read().unwrap();

        let val = guard.get(&reward_id.id);
        match val {
            Some(x) => Ok(x.clone()),
            None => Err(DataStoreError::NotFound {
                id: reward_id.id
            })
        }
    }

    fn delete_reward(&self, reward_id: RewardId) -> Result<(), DataStoreError> {
        let mut guard = self.rewards.write().unwrap();

        let result = guard.remove(&reward_id.id);

        match result {
            Some(_) => Ok(()),
            None => Err(DataStoreError::NotFound {
                id: reward_id.id
            }),
        }
    }

    fn update_reward(&self, reward: Reward) -> Result<Reward, DataStoreError> {
        let mut guard = self.rewards.write().unwrap();


        let exists = guard.contains_key(&reward.id);
        if exists == false {
            return Err(DataStoreError::NotFound {
                id: reward.id
            });
        }

        guard.insert(reward.id, reward.clone());
        Ok(reward.clone())
    }

    fn add_reward(&self, reward: Reward) -> Result<Reward, DataStoreError> {
        let max_id = self.max_id.fetch_add(1, atomic::Ordering::SeqCst);
        let mut guard = self.rewards.write().unwrap();
        let mut r = reward.clone();
        r.id = max_id;
        guard.insert(max_id, r.clone());
        Ok(r)
    }

    fn get_all_rewards(&self, user_id: UserId) -> Result<RewardList, DataStoreError> {
        let lock = self.rewards.read().unwrap();

        let list: Vec<Reward> = lock.values().filter(|r| r.owner.clone().unwrap() != user_id).map(|r| r.clone()).collect();

        Ok(RewardList { reward: list })
    }
}
