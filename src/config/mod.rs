use std::fs;

use serde::{Deserialize, Serialize};
use anyhow::{Context, Result};





#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub nats_url: String,
    pub monster_service_url: String,
    pub item_service_url: String,
    pub battle_service_url: String,
}

impl Config {
    pub fn load_config(path: &str) -> Result<Config> {
        let contents = fs::read_to_string(path).with_context(|| format!("could not open {}", path.to_string()))?;

        let config: Config = serde_json::from_str(&contents).with_context(|| format!("could not parse {}",path.to_string()))?;

        Ok(config)
    }
}
