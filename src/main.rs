#[macro_use]
extern crate log;
extern crate pretty_env_logger;

use proto::battle::{Battle};
use proto::battle::battle_service_client::BattleServiceClient;
use proto::battle::battle_state::State;
use proto::common::{BattleId, ResponseStatus, RewardId, UserId};
use proto::item::{AddItemToInventoryCall, ItemIdList, ItemType};
use proto::item::item_service_client::ItemServiceClient;
use proto::monster;
use proto::reward::{Reward, RewardList};
use proto::reward::reward_service_server::{RewardService, RewardServiceServer};
use tonic::{Request, Response, Status};
use tonic::transport::{Channel, Server};

use crate::repository::RewardRepository;

mod repository;
mod config;

struct RewardServiceState {
    repo: Box<dyn RewardRepository + Sync + Send>,
    item_client: ItemServiceClient<Channel>,
    battle_client: BattleServiceClient<Channel>,
}

impl RewardServiceState {}

#[tonic::async_trait]
impl RewardService for RewardServiceState {
    async fn create_reward(&self, request: Request<Battle>) -> Result<Response<Reward>, Status> {
        let battle = request.get_ref().clone();

        let blob = monster::Activity {
            activity: Some(monster::activity::Activity::BattleId(BattleId {
                id: battle.id,
            })),
        };

        let winning_team = match battle.state.unwrap().state.unwrap() {
            State::ActiveBattle(_) => {
                return Err(Status::not_found("Battle is not over"));
            }
            State::End(e) => e.winner,
        };


        let mut it_client = self.item_client.clone();
        let response = it_client
            .create_item_by_type(ItemType {
                r#type: String::from("pizza"),
            })
            .await?;
        println!("{:?}", response);

        let reward = Reward {
            id: 0,
            owner: winning_team,
            activity: Some(blob),
            item_list: Some(ItemIdList {
                item_ids: vec![response.into_inner()]
            }),
            collected: false,
        };
        let res = self.repo.add_reward(reward.clone()).unwrap();

        let val = self.repo.get_reward(RewardId { id: res.id }).unwrap();

        println!("stuff {} is added to field", val.id);

        Ok(Response::new(val))
    }
    async fn get_reward(&self, request: Request<RewardId>) -> Result<Response<Reward>, Status> {
        let req = request.get_ref().clone();
        let reward = self.repo.get_reward(req.clone()).map_err(|e| {
            warn!("could not find reward for id {}\nerror: {}", req.id, e);
            return Status::not_found("not found");
        })?;

        Ok(Response::new(reward.clone()))
    }
    async fn collect_reward(
        &self,
        request: Request<RewardId>,
    ) -> Result<Response<ResponseStatus>, Status> {
        let req = request.get_ref().clone();
        let reward = self.repo.get_reward(req.clone()).map_err(|e| {
            warn!("could not find reward for id {}\nerror: {}", req.id, e);
            return Status::not_found("not found");
        })?;
        let mut new_reward = reward.clone();

        if new_reward.collected == true {
            error!("reward {} was already collected", new_reward.id);
            Status::invalid_argument("was already collected");
        }

        info!("collect reward {}", new_reward.id);



        let item_list = reward.item_list.ok_or_else(|| Status::not_found("could not find stuff"))?;
        let mut item_client = self.item_client.clone();
        for i in item_list.item_ids {
            let call = AddItemToInventoryCall {
                user_id: new_reward.owner.clone(),
                item_id: Some(i),
            };
            let _response = item_client.add_item_to_inventory(call).await?;

            //Todo future error case handling
        }


        new_reward.collected = true;
        self.repo.update_reward(new_reward).map_err(|e| {
            warn!("could not find reward for id {}\nerror: {}", req.id, e);
            return Status::not_found("not found");
        })?;

        Ok(Response::new(ResponseStatus {
            success: true
        }))
    }

    async fn get_all_rewards_by_user(&self, request: Request<UserId>) -> Result<Response<RewardList>, Status> {
            let result=self.repo.get_all_rewards(request.into_inner().clone());

        match result{
            Ok(r) => Ok(Response::new(r.clone())),
            Err(e) =>{
                warn!("could not fidn any rewards \nerror: {}",e);
                return Err(Status::not_found("not found"));
            }

        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let reward_repo = repository::inmemory::Store::new();
    pretty_env_logger::init();

    let config = config::Config::load_config("res/dev_config.json")?;

    let addr = "127.0.0.1:50053".parse().unwrap();

    let item_client = ItemServiceClient::connect(config.item_service_url).await?;
    let battle_client = BattleServiceClient::connect(config.battle_service_url).await?;

    let reward_service = RewardServiceState {
        repo: Box::new(reward_repo),
        item_client: item_client.clone(),
        battle_client: battle_client.clone(),
    };

    let svc = RewardServiceServer::new(reward_service);

    info!("reward-service listening on {}", addr);

    Server::builder().add_service(svc).serve(addr).await?;
    Ok(())
}
